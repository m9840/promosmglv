<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('tittle')
    <link rel="icon" type="image/x-icon" href="{{url("/landing_page/assets/img/logo.png")}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	@stack('js')
    {{-- //coba2 --}}
    {{-- <link href="{{url('admin/css/sb-admin-2.min.css')}}" rel="stylesheet"> --}}
    <style type="css">
        .card-body{
            box-shadow: 0px 0px 20px 7px rgba(0,0,0,0,1);
        }
    </style>
    <style>
        .body{
            background: rgb(27, 10, 91);
        }
        .parallax {
          /* The image used */
          /* background-image: url("https://www.panorama-jtb.com/themes/basic-v3/assets/uploads/content30/xBandung-Banner1280x500px.jpg.pagespeed.ic.-wx0M3NVKM.webp"); */
          /* background-image: url("img_parallax.jpg"); */
        
          /* Set a specific height */
          min-height: 500px; 
        
          /* Create the parallax scrolling effect */
          background-attachment: fixed;
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
        }
        </style>
</head>
<body>
    <!-- As a link -->
   @include('layouts.header')
   <div style="margin:30px"></div>
   {{-- <div class="bg-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/76.jpg');height: 100vh"></div> --}}
    <div class="container">
        <!-- iklan 1 -->
        <div>
            <img align="center" alt="I'm an image" border="0" class="center" src="https://s0.2mdn.net/simgad/4185297073760667077" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%;  display: block;" title="I'm an image" >
        </div>
        <!-- end iklan 1 -->
        {{-- <div >
            <img align="center" alt="I'm an image" border="0" class="center fixedwidth" src="https://drive.google.com/uc?export=view&id=1ur2TmerErAUhbWqBUfB1qoCBbnbKIcFQ" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%;  display: block;" title="I'm an image" width="352">
        </div> --}}
        <div class="row">
            <div class="col-sm-12" style="">
                {{-- <img align="center" alt="I'm an image" border="0" class="center fixedwidth" src="https://drive.google.com/uc?export=view&id=1ur2TmerErAUhbWqBUfB1qoCBbnbKIcFQ" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%;  display: block;" title="I'm an image" width="352"> --}}
                {{-- carousel --}}
                @yield('jumbotron')
                {{-- size picture 1.440x700px --}}
                
                {{-- //carousel --}}
            </div>
        </div>
        {{-- @yield('middlebar') --}}
        <!-- iklan 2 -->
       {{-- @yield('iklan2') --}}
        <!-- end iklan 2 -->
        <!-- content -->
    </div>
    <div class="parallax">
        <div class="container">
            @yield('content')
            @yield('mbuh')
        <!-- end content -->
        </div>
        <footer class="bg-light bs-docs-section d-none d-sm-block" style="padding: 20px;">
            <div class="container" align="center">
                <h3>{{env('FOOTER_NAME')}}</h3>
                <p>{{env('FOOTER_CONTACT')}}</p>
            </div>
        </footer>
    </div>
    @stack('posting')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    {{-- <script src="{{url('admin/js/sb-admin-2.min.js')}}"></script> --}}
</body>
</html>