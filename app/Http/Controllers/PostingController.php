<?php

namespace App\Http\Controllers;

use App\Models\Posting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostingController extends Controller
{
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            // picture besar di gdrive, tumbnail di lokal
            $files=$request->file('file');
            // $filename=$files->getClientOriginalName();
            $input['imagename']=time().'.'.$files->extension();
            $filePath=$files->getRealPath();
            // // $path=$files->store('public/gallery',$files->getClientOriginalName());
            Posting::create([
                'email'=>$request->email,
                'no_wa'=>$request->no_wa,
                'tittle'=>$request->tittle,
                'isi'=>$request->isi,
                'picture_name'=>$input['imagename'],
            ]);
            // $input['imagename']=time().'.'.$files->extension();
            $destPath=public_path('/tumbnail');
            //send notif telegram
            $message="Update: no ".$request->no_wa." make posting with tittle : ".$request->tittle;
            $response = \Http::post("https://api.telegram.org/bot5710031602:AAE08BO7zbHT6NZx35y2VedPgXEdNXiqzq4/sendMessage?chat_id=1496086415&text=".$message)->json();
          
            //save gdrive
            $path=Storage::cloud()->put($input['imagename'], fopen($filePath, 'r+'));
            //save local
            $img=Image::make($files->path());
            $img->resize(200, null,function($constraint){
                $constraint->aspectRatio();
            })->save($destPath.'/'.$input['imagename']);
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return back()->with('fail','something went wrong');
            // something went wrong
        }
        // dd($img);
        // $img=Image::make($request->file('photo'))->resize(115, 115)->save('uploads/' . $nama_gambar);
        return redirect()->route("home");
    }

    public function show(Posting $posting,Request $request)
    {
        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
        //get image from local
        $data=DB::select('select * from postings where id = ?', [$request->id]);
        $gd=$contents->where('name', '=', $data[0]->picture_name);
        foreach ($gd as $d ){
            $data[0]->picture_name=$d['path'];
        }
        return view('contents.details.index', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function edit(Posting $posting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posting $posting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posting $posting)
    {
        //
    }
}
