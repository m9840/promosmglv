<?php

namespace App\Http\Controllers;

use App\Models\Landing;
use App\Models\Posting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get image from gdrive
        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
        //get image from local

        $data=Posting::where("status",1)
        ->get();
        // dump($data[0]->isi);
        // dd(count($data));

        for ($i=0; $i<count($data) ; $i++) { 
            $data[$i]->isi=substr(strip_tags($data[$i]->isi),0,100)."...";
            $data[$i]->picture_name="/tumbnail"."/". $data[$i]->picture_name;
            // dump($data[$i]->picture_name);
            // $gd=$contents->where('name', '=', $data[$i]->picture_name);

            // foreach ($gd as $gdata){
            //     if ($data[$i]->picture_name=$gdata['name']){
            //         // dump($data[$i]->picture_name." :::: ".$gdata['name']);
            //         $data[$i]->picture_name=$gdata['path'];
            //     }   
            // }

        }
        $keyWord="";
        // dd("o");
        return view("contents.landing.home",compact('data','keyWord'));
    }
    public function addPost()
    {

        return view("contents.newPost.index");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $keyWord=$request->search;
        $data=Posting::where("tittle","ilike","%".$request->search."%")
        ->orWhere("isi","ilike","%".$request->search."%")
        ->get();
        // dd($data);
        for ($i=0; $i<count($data) ; $i++) { 
            $data[$i]->isi=substr(strip_tags($data[$i]->isi),0,100)."...";
            $data[$i]->picture_name="/tumbnail"."/". $data[$i]->picture_name;
        }
        return view("contents.landing.home",compact('data','keyWord'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Landing  $landing
     * @return \Illuminate\Http\Response
     */
    public function show(Landing $landing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Landing  $landing
     * @return \Illuminate\Http\Response
     */
    public function edit(Landing $landing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Landing  $landing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Landing $landing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Landing  $landing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Landing $landing)
    {
        //
    }
}
